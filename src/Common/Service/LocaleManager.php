<?php

declare(strict_types=1);

namespace Nordcode\SyliusTranslationsPlugin\Common\Service;

use Lexik\Bundle\TranslationBundle\Manager\LocaleManager as BaseLocaleManager;
use Lexik\Bundle\TranslationBundle\Manager\LocaleManagerInterface;
use Sylius\Component\Locale\Model\LocaleInterface;

class LocaleManager extends BaseLocaleManager implements LocaleManagerInterface
{
    protected $managedLocales = [];

    public function __construct(array $managedLocales)
    {
        $this->managedLocales = $managedLocales;
    }

    public function getLocales(): array
    {
        if (count($this->managedLocales) > 0 && $this->managedLocales[0] instanceof LocaleInterface) {
            return array_map(function (LocaleInterface $locale) {
                return $locale->getCode();
            }, $this->managedLocales);
        }

        return $this->managedLocales;
    }
}
