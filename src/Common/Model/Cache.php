<?php

declare(strict_types=1);

namespace Nordcode\SyliusTranslationsPlugin\Common\Model;

class Cache
{
    public const CACHE_TAG = 'messages';
}
