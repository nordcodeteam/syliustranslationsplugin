<?php

declare(strict_types=1);

namespace Nordcode\SyliusTranslationsPlugin\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\OutOfBoundsException;
use Symfony\Component\DependencyInjection\Reference;

class TranslationResourceFilesPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('translator.default')) {
            return;
        }

        $translationFiles = $this->getTranslationFilesFromAddResourceCalls($container);
        $translationFiles = array_merge($translationFiles, $this->getTranslationFiles($container));

        $container->getDefinition('nordcode_sylius_translations_plugin.common.translation_finder')
            ->replaceArgument(0, $translationFiles);

        if (!$container->has('nordcode_sylius_translations_plugin.api.translation_dumper')) {
            return;
        }
        $dumper = $container->getDefinition('nordcode_sylius_translations_plugin.api.translation_dumper');
        foreach ($container->findTaggedServiceIds('translation.loader') as $id => $tags) {
            foreach ($tags as $tag) {
                $dumper->addMethodCall('addLoader', [$tag['alias'], new Reference($id)]);
            }
        }
    }

    /**
     * @param ContainerBuilder $container
     *
     * @return array
     */
    private function getTranslationFilesFromAddResourceCalls(ContainerBuilder $container)
    {
        $translationFiles = [];

        $methodCalls = $container->findDefinition('translator.default')->getMethodCalls();
        foreach ($methodCalls as $methodCall) {
            if ($methodCall[0] === 'addResource') {
                $locale = $methodCall[1][2];
                $filename = $methodCall[1][1];

                if (!isset($translationFiles[$locale])) {
                    $translationFiles[$locale] = [];
                }

                $translationFiles[$locale][] = $filename;
            }
        }

        return $translationFiles;
    }

    /**
     * @param ContainerBuilder $container
     *
     * @return array
     */
    private function getTranslationFiles(ContainerBuilder $container)
    {
        $translationFiles = [];
        $translator = $container->findDefinition('translator.default');

        try {
            $translatorOptions = $translator->getArgument(4);
        } catch (OutOfBoundsException $e) {
            $translatorOptions = [];
        }

        $translatorOptions = array_merge($translatorOptions, $translator->getArgument(3));

        if (isset($translatorOptions['resource_files'])) {
            $translationFiles = $translatorOptions['resource_files'];
        }

        return $translationFiles;
    }
}
