<?php

namespace Nordcode\SyliusTranslationsPlugin\Api\Controller;

use Nordcode\SyliusTranslationsPlugin\Api\Service\TranslationDumper;
use Nordcode\SyliusTranslationsPlugin\Api\Service\TranslationDumperInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TranslationsController extends AbstractController
{
    /**
     * @var TranslationDumperInterface
     */
    private $translationDumper;

    /**
     * TranslationsController constructor.
     *
     * @param TranslationDumperInterface $translationDumper
     */
    public function __construct(TranslationDumperInterface $translationDumper)
    {
        $this->translationDumper = $translationDumper;
    }

    /**
     * @param Request $request
     * @param         $domain
     * @param         $_format
     *
     * @return Response
     */
    public function getTranslationsAction(Request $request, $domain, $_format)
    {
        return new JsonResponse($this->translationDumper->dump($request->getLocale(), $request->attributes->get('domain')));
    }
}
