<?php

namespace Nordcode\SyliusTranslationsPlugin\Api\Service;

use Symfony\Component\Translation\Loader\LoaderInterface;

interface TranslationDumperInterface
{
    /**
     * Add a translation loader if it does not exist. Used in compiler pass
     *
     * @param string          $id     The loader id.
     * @param LoaderInterface $loader A translation loader.
     */
    public function addLoader($id, $loader): void;

    public function dump(?string $locale = null, ?string $domain = null);
}
