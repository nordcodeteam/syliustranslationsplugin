<?php

declare(strict_types=1);

namespace Nordcode\SyliusTranslationsPlugin\Api\Service;

use Nordcode\SyliusTranslationsPlugin\Common\Service\TranslationFinder;
use Symfony\Component\Translation\Loader\LoaderInterface;
use Symfony\Component\Translation\MessageCatalogue;
use Symfony\Contracts\Translation\TranslatorInterface;

class TranslationDumper implements TranslationDumperInterface
{
    /** @var TranslatorInterface */
    private $translator;

    /**
     * @var TranslationFinder
     */
    private $finder;

    /**
     * @var array
     */
    private $loaders = [];

    /**
     * @var array List of locales translations to dump
     */
    private $activeLocales;

    /**
     * @var array List of domains translations to dump
     */
    private $activeDomains;

    public function __construct(TranslatorInterface $translator, TranslationFinder $finder, array $loaders, array $activeLocales, array $activeDomains)
    {
        $this->translator = $translator;
        $this->finder = $finder;
        $this->loaders = $loaders;
        $this->activeLocales = $activeLocales;
        $this->activeDomains = $activeDomains;
    }

    /**
     * Add a translation loader if it does not exist. Used in compiler pass
     *
     * @param string          $id     The loader id.
     * @param LoaderInterface $loader A translation loader.
     */
    public function addLoader($id, $loader): void
    {
        if (!array_key_exists($id, $this->loaders)) {
            $this->loaders[$id] = $loader;
        }
    }

    public function dump(?string $locale = null, ?string $domain = null)
    {
        return json_encode($this->getTranslations($locale, $domain), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    private function getTranslations(?string $locale = null, ?string $domain = null): array
    {
        $locale = $locale ?: $this->activeLocales[0];
        $domain = $domain ?: 'messages';

        /** @var MessageCatalogue $catalogue */
        $catalogue = $this->translator->getCatalogue($locale);
        while (0 === count($catalogue->all($domain))) {
            if ($cat = $catalogue->getFallbackCatalogue()) {
                $catalogue = $cat;
            } else {
                break;
            }
        }

        $translations[$locale] = $catalogue->all($domain);

        return $translations;
    }
}
