<?php

declare(strict_types=1);

namespace Nordcode\SyliusTranslationsPlugin\Admin\Twig;

use Nordcode\SyliusTranslationsPlugin\Api\Service\TranslationDumperInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TranslationsExtension extends AbstractExtension
{
    private $translationDumper;

    public function __construct(TranslationDumperInterface $translationDumper)
    {
        $this->translationDumper = $translationDumper;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('dump_translations', [$this, 'dumpTranslations']),
        ];
    }

    public function dumpTranslations(?string $locale, ?string $domain = null): string
    {
        return $this->translationDumper->dump($locale, $domain);
    }
}
