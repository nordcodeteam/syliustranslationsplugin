<?php

declare(strict_types=1);

namespace Nordcode\SyliusTranslationsPlugin\Admin\EventListener;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

final class AdminMenuListener
{
    public function addAdminMenuItems(MenuBuilderEvent $event): void
    {
        $menu = $event->getMenu();
        $menu
            ->getChild('catalog')
            ->addChild(
                'nordcode_sylius_translations_plugin.ui.translations',
                ['route' => 'nordcode_sylius_translations_plugin_admin_translate']
            )
            ->setLabelAttribute('icon', 'pencil alternate');;
    }
}
