<?php

declare(strict_types=1);

namespace Nordcode\SyliusTranslationsPlugin\Admin\Controller;

use Nordcode\SyliusTranslationsPlugin\Common\Model\Cache;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Locale\Model\LocaleInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class TranslationsController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var TagAwareAdapterInterface
     */
    private $cache;

    /**
     * @var EntityRepository
     */
    private $localeRepository;

    /**
     * TranslationsController constructor.
     *
     * @param TranslatorInterface      $translator
     * @param TagAwareAdapterInterface $cache
     * @param EntityRepository         $localeRepository
     */
    public function __construct(TranslatorInterface $translator, TagAwareAdapterInterface $cache, EntityRepository $localeRepository)
    {
        $this->translator = $translator;
        $this->cache = $cache;
        $this->localeRepository = $localeRepository;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse|RedirectResponse
     */
    public function invalidateCacheAction(Request $request): Response
    {
        $locales = array_map(function (LocaleInterface $locale) {
            return $locale->getCode();
        }, $this->localeRepository->findAll());

        $this->translator->removeLocalesCacheFiles($locales);
        $this->cache->invalidateTags([Cache::CACHE_TAG]);

        $message = $this->translator->trans('translations.cache_removed', [], 'LexikTranslationBundle');

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse(['message' => $message]);
        }

        $this->get('session')->getFlashBag()->add('success', $message);

        return $this->redirect($this->generateUrl('lexik_translation_grid'));
    }
}
