<?php

declare(strict_types=1);

namespace Nordcode\SyliusTranslationsPlugin;

use Nordcode\SyliusTranslationsPlugin\DependencyInjection\Compiler\TranslationResourceFilesPass;
use Sylius\Bundle\CoreBundle\Application\SyliusPluginTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class NordcodeSyliusTranslationsPlugin extends Bundle
{
    use SyliusPluginTrait;

    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new TranslationResourceFilesPass());
    }
}
